/* eslint-disable jsx-a11y/alt-text */

import React from 'react';

import './App.css';
import tasks from "./data/tasks.json"

import Tasks from './components/Tasks';

class App extends React.Component{

  state = {
    tasks:tasks
  }

  render(){
    return (<div> 
        <Tasks tasks={this.state.tasks}/>
      </div>
    )
  }
}

export default App;
