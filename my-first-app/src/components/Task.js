import React from "react"


class task extends React.Component{
    render(){
        const{task}=this.props

        return <div>
            {task.id} - {task.title} - {task.description} - {task.isDone}
            <input type="checkbox"/>
            <button> x </button>
        </div>
    }
}

export default task